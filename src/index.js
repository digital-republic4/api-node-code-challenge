const express =  require('express');
require('dotenv').config()
const route = require('./routes/routes')

const app = express()
app.use(express.json())
app.use(route);

module.exports = app;