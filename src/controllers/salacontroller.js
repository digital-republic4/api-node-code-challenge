const salaModel = require('../models/salaModel');
const paredeModel = require('../models/paredeModel');

const index = async (req,res) => {
    const salas = await salaModel.getAll();
    res.status(200).send(salas)
};

const create = async (req,res) => {
    const id = req.params['id']
    var metros_total = 0;
    var litros18 = 0;
    var litros3 = 0;
    var litros2 = 0;
    var litros05 =0;
    const paredes = await paredeModel.getBySala(id);
    
    paredes.forEach(parede => {  
            metros_total = metros_total + parede['metro_quadrado'];   
    });
    
    
    
    if(metros_total >= 18){
        litros18 = Math.floor(metros_total/18);
        console.log(litros18)

        resto = metros_total - (18 * litros18);
        
        if(resto >= 3){
            if(resto <= 3.6){
                litros3 = 1;
            }else{
                litros3 = Math.floor(resto/3.6);
                resto= resto - (3.6 * litros3);
                if(resto != 0){
                    litros05 = 1;
                }
            }
        }else if(resto >= 2){
            if(resto <= 2.5){
                litros2 = 1;
            }else {
                litros2 = Math.floor(resto/2.5);
                resto = resto - (2.5 * litros2);
                if(resto != 0){
                    litros05 = 1;
                }
            }
            
        }else{
            if(resto != 0){
            
            litros05 = Math.floor(resto/0.5);
            resto = resto - (0.5 * litros05);  
            }
        }
    }

    const litros = litros18 + " latas de 18 L, " + litros3 + " latas de 3,6 L, " + litros2 + " latas de 2,5L, " + litros05 + " latas de 0,5 L";
    
    salaModel.update(metros_total, litros, id);
    
    res.status(200).send({
        message: litros
    })
}

module.exports = {
    index, create
}
