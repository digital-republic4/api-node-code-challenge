const salaModel = require('../models/salaModel');
const paredeModel = require('../models/paredeModel');

const create = (req,res) => {
    res.status(200).send("create Parede")
}

const store = async (req,res) => {
    const paredes = req.body
    
    await salaModel.createSala();
    const sala = await salaModel.getLast()
    console.log(sala[0]['id'])
    paredes.forEach(async parede => {
        
        const result = await paredeModel.createParede(
                    parede['altura'],
                    parede['largura'],
                    parede['metro_quadrado'],
                    parede['porta'],
                    parede['janela'],
                    sala[0]['id'])
                
    });
    
    res.status(201).json({
        "message": sala[0]['id']
    })
    
}

module.exports = {
    create,
    store
}