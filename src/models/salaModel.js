const connection = require('../connection')

const getAll = async () => {
    const [result] = await connection.execute('SELECT * from salas');
    return result
}
const getById = async (id) => {
    const [result] = await connection.execute('SELECT * from salas WHERE id = '+ id);
    return result
}
const createSala = async () => {
    const [result] = await connection.execute('INSERT INTO salas (metros_total, litros) VALUES (0,0)');
    return result
}

const getLast = async () =>{
    const [result] = await connection.execute('SELECT * FROM salas ORDER BY id DESC LIMIT 1');
    return result
    
}

const update = async (metros, litros, id) => {
    const [result] = await connection.execute('UPDATE salas SET metros_total = '+metros+ ', litros = "'+ litros + '" WHERE id = ' + id)
    return result
}
module.exports = {
    getAll, getById, update,createSala, getLast
}