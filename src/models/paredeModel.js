const connection = require('../connection')

const getAll = async () => {
    const [result] = await connection.execute('SELECT * from paredes');
    return result
}

const getBySala = async (id) => {
    const [result] = await connection.execute('SELECT * FROM paredes WHERE sala_id = '+id)
    
    return result
}

const createParede = async (altura,largura,metro_quadrado,porta,janela,sala_id) => {
    const [result] = await connection.execute('INSERT INTO paredes (altura, largura, metro_quadrado,porta, janela, sala_id) VALUES ('+altura+','+ largura +','+ metro_quadrado +','+ porta +','+ janela +','+ sala_id + ')');
    return result
}

module.exports = {
    getAll, getBySala, createParede
}