const express = require('express')
const salaController = require('../controllers/salacontroller')
const paredeController = require('../controllers/paredecontroller')
const route = express.Router();

route.get('/', salaController.index)
route.get('/create/:id', salaController.create)
route.get('/parede/create', paredeController.create)
route.post('/parede/store', paredeController.store)

module.exports = route;