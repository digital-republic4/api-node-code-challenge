# Code Challenge | Murillo Florentino

Olá, fiz esse code challenge em três diretórios, o primeiro é completo em Laravel, o segundo é uma API em node para ser usada no APP, e o terceiro é um APP feito em React Native

## Laravel
Logo após baixar os diretórios, precisaremos usar um comando no terminal da aplicação
`php artisan migrate`
Esse comando ira criar e popular o banco de dados que será utilizado tanto no Laravel quanto na API. Após esse comando vamos iniciar o servidor no próprio laravel.
`php artisan serve`

##  API em Node
Para iniciar a API primeiro vamos dar o comando para instalar as bibliotecas necessárias:
`npm install`
Caso deseja alterar alguma configuração do banco de dados ou porta do servidor, no arquivo **.env** você poderá alterar essas informações.
Depois de configurado, podemos inciar o servidor utilizando o comando.
`npm start`

##  React Native
Para iniciar o App primeiro vamos dar o comando para instalar as bibliotecas necessárias:
`npm install`
Para acessar a API que foi criada em Node, será necessário alterar algumas informações no **.env** vamos precisar colocar o IP da maquina que está rodando o servidor e a porta, Exemplo: 
`http://192.168.0.1:3333/`
Depois de configurado, podemos inciar o servidor utilizando o comando.
`npm start`




